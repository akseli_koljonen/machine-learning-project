## Abstract

This project involves investigating and constructing machine learning methods for predicting genre of a song from raw audio signals. The raw audio signals were transformed to features MFCCs (Mel Frequency Cepstral Coefficients), chroma values, and rhythm patterns. Machine learning algorithm was trained with smaller feature set that had been labelled. After training the algorithm it was used to predict genres for larger feature set by mapping features to probability of song being a specific genre. In the first chapter we introduce the problem and premises in more detail. Second chapter will describe the data and data structures that were involved in the project. In third chapter describes groups approach to the problem and the process how the solution was formed. After that we will see the results and how did they compare against training data. In the last chapter conclusions from the results are presented and discussed.

## 1. Introduction

Purpose of this project is to design and create a complete machine learning solution to identify music genre of songs. This means that the machine learning problem is about classification. Classification means that we are given data points that are to be pointed to classes (in our case music genres). We are doing this by first training a machine learning model to estimate how likely it is that some data sample belongs to certain class. This is done by feeding a model with samples which class we know beforehand. After training is complete the trained model estimates the probabilities with which data point belongs to each class. Class with the highest probability is chosen for the data. The problem with classifying music is that the line between genres is quite blurry which makes choosing of the model difficult.

We were interested in how machine learning problems are approached, what restrictions and challenges do they present and what options are out there. More specifically how do we perform classification for songs genre that can be hard even for human. Practical experience gained from the project cannot be overlooked. Actually, implementing and going through the hoops to provide something that results in satisfactory outcome can teach us things that would be hard to learn from readings and by our intuition.

In this project we were focusing on the practicalities such as what kind of data are we working with and how does that impact our solution. This includes both the data that is used for training the model and the data that needs to be classified. We are also exploring what different classifier solutions there are and how do they perform in our task.

Machine learning and artificial intelligence are rapidly evolving fields that are more and more integrated to our daily lives. It is important to understand how these technologies work and what challenges they have. Machine learning and advances in technology give us a new way to solve problems and create systems that previously were out of reach. These are the motivations behind the project.

## 3. Methods and experiments

Our approach included simple preprocessing, linear transformation methods and regression.

At first, we investigated the data in order to find incorrect values which could have negative impact on our classification. Due to this, we split both the training and test data matrices into 3 different matrices: rhythm, chroma and MFCC. We had to decide, what kind of value is "incorrect", thus we define a threshold value which is the mean of the ith row multiplied with 20. If the value is greater than our threshold, we change the incorrect value to mean of the ith row. This way these large values will not skew our data. When all the incorrect values were chaneged, we combined the matrices back to training and test matrices.

After the first preprocessing step, we standardized our data. We used StandardScaler function in order to transform the data such that its distribution will have a mean value 0 and standard deviation of 1. Given the distribution of the data, each value in the dataset will have the sample mean value subtracted, and then divided by the standard deviation of the whole dataset. Usually standardization is a requirement for the optimal performance of many machine learning algorithms since if the scales for different features are wildly different, it can have a knock-on effect on the ability to learn.

When all of the preprocessing steps were done, we used linear transformation methods to "simplify" our data. PCA returns the directions i.e. principal components which will maximize the variance of the data. LDA also aims to find the directions that maximize the separation i.e. discrimination between different classes. In both methods, we will lose information but these methods will increase the performance of our algorithm. We used both methods separately in order to find the optimal result.

Next step was to train our algorithm and predict the labels for test data. For this step, we used logistic regression. Logistic regression outputs the probability that the given input point belongs to a certain class. We tried many different classifier and regression algorithms and logistic regression gave the best results. The Logistic regression of SciKit library uses many parameters such as solver, random_state and multi_class. In order to find the best parameters, we tried to changed the parameters and find the best ones. When the best parameters were found, we used these parameters to train the algorithm with the real training data and predicted the labels of the real test data.

Since the data was imbalanced as we can see from the figure 1, the data should have been oversampled in the pre-processing stage. Oversampling the testing data was not possible since the size of the testing labels had to be constant for the competition. We tried oversampling for the training data and our accuracy increased significantly.

For the evaluation methodology we used cross validation where the training data is split into random train and test subsets. With these subsets, we are able to train the algorithm and predict the test labels and see how well we trained the algorithm.

## Results

Local testing was done with the training data. Training data was split to local training data and test data. With these data sets, the training accuracy was 65-70%. When we made the predictions to the real test data and uploaded it to Kaggle, the accuracy was about 65%. Our training accuracy fluctuated even if we ran the program with same algorithms. This was due to the parameter used in the cross validation function train_test_split(). It will produce a random split for the training data if the random_state parameter is not set.

When we oversampled the local training data, the accuracy was over 75%. Oversampling the testing data was not possible, since the size of the training labels would have been too large for Kaggle.

The accuracy was usually little bit higher with local data (without oversampling). However, we did a lot more accuracy calculations with the local training data since on Kaggle there was a upload limitation (4 uploads per day).

## How to run

Just run the main.py and you should be good. You can change the parameters in the source code.
