import pandas as pd
import importlib
import numpy as np
import csv
import matplotlib.pyplot as plt
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score, log_loss
from sklearn.decomposition import PCA
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.multiclass import OneVsOneClassifier
from sklearn.svm import LinearSVC
from sklearn import preprocessing
from sklearn.svm import SVC
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.cluster import KMeans
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from scipy import stats

finalResult = False

def main():
    global finalResult
    #classifiers(X_train_stn, X_test_stn, y_train,y_test)
    #OnevsOne(X_train_stn, X_test_stn, y_train,y_test)
    best_pca_result = 0
    i = 0
    threshold = 69.5

    while i < 1000:
        # Get the data and standardize it
        X_train, X_train_tmp, X_test, y_train, y_test, X_test_real = getData()
        X_train_stn, X_train_tmp_stn, X_test_stn, X_test_real_stn = standardData(X_train,X_train_tmp,X_test,X_test_real)
        best_pca_result, best_pca, best_c_pca, best_solver_PCA, best_multi_class_PCA = testPCA(X_train_tmp_stn, X_test_stn,y_train,y_test)
        i += 1
        print(i, best_pca_result,best_c_pca)

        # If the result is better than the threshold, calculate the predictions to the real data
        if best_pca_result > threshold:
            threshold = best_pca_result
            print("Best result: %f with PCA: %d, C: %f and solver: %s" % (best_pca_result, best_pca, best_c_pca, best_solver_PCA))
            finalResult = True
            X_train_pca, X_test_real_pca = principalComponentAnalysis(X_train_stn, X_test_real_stn, None)
            y_pred_real = logisticRegression(X_train_pca, X_test_real_pca, y_train, best_c_pca, best_solver_PCA, "auto")
            produceOutputCsv(y_pred_real)
            y_pred_real_list = y_pred_real.tolist()
            produceOutputTxt(y_pred_real_list)

def getData():
    df_test = pd.read_csv("data/test_data.csv",header=None)
    df_train = pd.read_csv("data/train_data.csv",header=None)
    df_labels = pd.read_csv("data/train_labels.csv",header=None)
    #describeData(df_test, df_train)
    X_train = df_train.values
    X_test_real = df_test.values
    y_train = df_labels.values
    X_train, X_test_real = preProcess(X_train, X_test_real)
    X_train_tmp, X_test, y_train, y_test = train_test_split(X_train, y_train, test_size=0.2)
    return X_train,X_train_tmp, X_test, y_train, y_test, X_test_real
    #print(X_train.shape, y_train.shape)

def describeData(df_test, df_train):
    rhythm_train = df_test.loc[:,0:167]
    chroma_train = df_test.loc[:,167:215]
    MFCC_train = df_test.loc[:,262:]
    series_rhythm_train = pd.Series(rhythm_train.values.ravel())
    #print("rhythm_train", series_rhythm_train.describe())
    series_chroma_train = pd.Series(chroma_train.values.ravel())
    #print("chroma_train", series_chroma_train.describe())
    series_MFCC_train = pd.Series(MFCC_train.values.ravel())
    #print("MFCC_train", series_MFCC_train.describe())


def OnevsOne(X_train, X_test, y_train,y_test):
    ovo = OneVsOneClassifier(LinearSVC(random_state=2,max_iter=50000))
    y_pred = ovo.fit(X_train, y_train.ravel()).predict(X_test)
    result = checkResult(y_pred, y_test)
    print(result)

def linearDiscriminantAnalysis(X_train, X_test, y_train, lda_value):
    lda = LinearDiscriminantAnalysis(n_components = lda_value)
    X_train = lda.fit_transform(X_train, y_train.ravel())
    X_test = lda.transform(X_test)
    return X_train, X_test

def standardData(X_train,X_train_tmp,X_test,X_test_real):
    scaler = StandardScaler()
    scaler_tmp = StandardScaler()
    scaler.fit(X_train)
    scaler_tmp.fit(X_train_tmp)
    X_train_tmp = scaler_tmp.fit_transform(X_train_tmp)
    X_test = scaler_tmp.transform(X_test)
    X_train = scaler.fit_transform(X_train)
    X_test_real = scaler.transform(X_test_real)
    return X_train, X_train_tmp,X_test, X_test_real

def principalComponentAnalysis(X_train,X_test,pca_value):
    if pca_value != None:
        pca = PCA(n_components = pca_value)
        X_train = pca.fit_transform(X_train)
        X_test = pca.transform(X_test)
    return X_train, X_test

def logisticRegression(X_train,X_test,y_train,c,solver,multi_class):
    logisticRegr = LogisticRegression(solver=solver, random_state=2, multi_class=multi_class,max_iter=5000, C=c, n_jobs=-1)
    logisticRegr.fit(X_train, y_train.ravel())
    y_pred = logisticRegr.predict(X_test)
    global finalResult
    if(finalResult):
        y_prob = logisticRegr.predict_proba(X_test)
        produceLogLossCsv(y_prob)
    return y_pred

def classifiers(X_train, X_test, y_train,y_test):
    learning_rate = 0.01
    forest = RandomForestClassifier(n_estimators=100, max_depth=2, random_state=0)
    y_pred_for = forest.fit(X_train, y_train.ravel()).predict(X_test)
    gradient = GradientBoostingClassifier(n_estimators=10, learning_rate = learning_rate, max_features=2, max_depth = 2, random_state = 0)
    gradient.fit(X_train, y_train.ravel())
    y_pred_grad = gradient.predict(X_test)
    clf = SVC()
    clf.fit(X_train, y_train.ravel())
    y_pred_clf = clf.predict(X_test)
    result_for = checkResult(y_pred_for, y_test)
    result_grad = checkResult(y_pred_grad, y_test)
    result_clf = checkResult(y_pred_clf, y_test)
    print(result_for,result_grad,result_clf)

def preProcess(X_train, X_test):
    rhythm_train = X_train[:,0:167]
    chroma_train = X_train[:,167:215]
    MFCC_train = X_train[:,216:]
    rhythm_test = X_test[:,0:167]
    chroma_test = X_test[:,167:215]
    MFCC_test = X_test[:,216:]
    #print(stats.describe(rhythm_train))
    #print(X_train.shape)
    #print(rhythm_train[0].mean(),chroma_train[0].mean(),MFCC_train[0].mean())
    #print(rhythm_train.shape,chroma_train.shape,MFCC_train.shape)
    for i in range(len(rhythm_train)):
        for j in range(len(rhythm_train[i])):
            if rhythm_train[i,j] >= rhythm_train[i].mean()*20:
                rhythm_train[i,j] = rhythm_train[i].mean()

    for i in range(len(chroma_train)):
        for j in range(len(chroma_train[i])):
            if chroma_train[i,j] >= chroma_train[i].mean()*20:
                chroma_train[i,j] = chroma_train[i].mean()

    for i in range(len(MFCC_train)):
        for j in range(len(MFCC_train[i])):
            if MFCC_train[i,j] >= MFCC_train[i].mean()*20:
                MFCC_train[i,j] = MFCC_train[i].mean()

    #print(rhythm_train.shape,chroma_train.shape,MFCC_train.shape)
    #print(max(rhythm_train[0]),max(chroma_train[0]), max(MFCC_train[0]))
    #print(rhythm_train.mean(),chroma_train.mean(),MFCC_train.mean())
    #print(rhythm_train[0].mean(),chroma_train[0].mean(),MFCC_train[0].mean())

    for i in range(len(rhythm_test)):
        for j in range(len(rhythm_test[i])):
            if rhythm_test[i,j] >= rhythm_test[i].mean()*20:
                rhythm_test[i,j] = rhythm_test[i].mean()

    for i in range(len(chroma_test)):
        for j in range(len(chroma_test[i])):
            if chroma_test[i,j] >= chroma_test[i].mean()*20:
                chroma_test[i,j] = chroma_test[i].mean()

    for i in range(len(MFCC_test)):
        for j in range(len(MFCC_test[i])):
            if MFCC_test[i,j] >= MFCC_test[i].mean()*20:
                MFCC_test[i,j] = MFCC_test[i].mean()

    X_train_a = np.hstack((rhythm_train,chroma_train))
    X_train = np.hstack((X_train_a,MFCC_train))
    X_test_a = np.hstack((rhythm_test,chroma_test))
    X_test = np.hstack((X_test_a,MFCC_test))
    return X_train, X_test

def testPCA(X_train_stn, X_test_stn,y_train,y_test):
    pca_values = [None]
    c_values = [0.2,0.1,0.5]
    solvers = ['newton-cg', 'lbfgs', 'liblinear', 'sag', 'saga']
    solvers = ['liblinear']
    best_result = 0
    for solver in solvers:
        for value in pca_values:
            for c in c_values:
                if solver == "liblinear":
                    multi_class = 'auto'
                else:
                    multi_class = 'multinomial'
                X_train_pca, X_test_pca = principalComponentAnalysis(X_train_stn,X_test_stn,value)
                y_pred = logisticRegression(X_train_pca,X_test_pca,y_train,c,solver,multi_class)
                result = checkResult(y_pred, y_test)
                if result > best_result:
                    #if pca_values == None:
                    #    best_no_pca = result
                    #    print("New best result: %f _WITHOUT_ PCA, C: %f and solver: %s" % (best_no_pca, best_c, best_solver))
                    #else:
                    best_result = result
                    best_pca = value
                    best_c = c
                    best_solver = solver
                    best_multi_class = multi_class
                    print("New best result: %f with PCA: %d, C: %f and solver: %s" % (best_result, 0, best_c, best_solver))
        print("PCA with %s done!" % (solver))
    print("PCA done!")
    return best_result, 0, best_c, best_solver, best_multi_class

def testLDA(X_train_stn, X_test_stn,y_train,y_test):
    LDA_values = [2, 4, 12, 24, 48, 96, 128]
    c_values = [0.2, 1, 2, 3, 4, 5, 6]
    solvers = ['newton-cg', 'lbfgs', 'liblinear', 'sag', 'saga']
    best_result = 0
    for solver in solvers:
        for value in LDA_values:
            for c in c_values:
                if solver == "liblinear":
                    multi_class = 'auto'
                else:
                    multi_class = 'multinomial'
                X_train_LDA, X_test_LDA = linearDiscriminantAnalysis(X_train_stn,X_test_stn,y_train, value)
                y_pred = logisticRegression(X_train_LDA,X_test_LDA,y_train,c,solver, multi_class)
                result = checkResult(y_pred, y_test)
                if result > best_result:
                    best_result = result
                    best_LDA = value
                    best_c = c
                    best_solver = solver
                    best_multi_class = multi_class
                    print("New best result: %f with LDA: %d, C: %f and solver: %s" % (best_result, best_LDA, best_c, best_solver))
        print("LDA with %s done!" % (solver))
    print("LDA done!")
    return best_result, best_LDA, best_c, best_solver,best_multi_class

def checkResult(y_pred, y_test):
    N = len(y_test)
    correct = 0
    for i in range((N)):
        if (y_test[i] == y_pred[i]):
            correct += 1
    accuracy = round(correct/N,4)*100
    return accuracy

def produceOutputTxt(y_pred):
    file_name = "out.txt"
    i = 0
    header = ["Sample_id","Sample_label"]
    with open(file_name, 'w') as f:
        for item in y_pred:
            if i == 0:
                f.write("%s%s%s\n" % (header[0],",",header[1]))
            else:
                f.write("%s%s%s\n" % (i,",",item))
            i += 1
    f.close()

def produceOutputCsv(y_pred):
    idx = list(range(1, len(y_pred)+1))
    df = pd.DataFrame(pd.DataFrame(
    {'Sample_id': idx,
     'Sample_label': y_pred
    }))
    df.to_csv("outAccuracy.csv", header=True, index=False)

def produceLogLossCsv(y_prob):
    print(np.shape(y_prob))
    print(y_prob)
    idx = list(range(1, np.shape(y_prob)[0] + 1))
    df = pd.DataFrame(pd.DataFrame(
        {'Sample_id': idx,
         'Class_1':[i[0] for i in y_prob],
         'Class_2':[i[1] for i in y_prob],
         'Class_3':[i[2] for i in y_prob],
         'Class_4':[i[3] for i in y_prob],
         'Class_5':[i[4] for i in y_prob],
         'Class_6':[i[5] for i in y_prob],
         'Class_7':[i[6] for i in y_prob],
         'Class_8':[i[7] for i in y_prob],
         'Class_9':[i[8] for i in y_prob],
         'Class_10':[i[9] for i in y_prob]
         }))
    df.to_csv("outLogLoss.csv", header=True, index=False)


if __name__ == "__main__":
    main()
